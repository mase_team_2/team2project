/*$(function() {
	$("#progressbar").progressbar();
	$("#progressbar").hide();
	$("#upload_btn").click(upload);
});*/

var rootUrl = "http://localhost:8080/Team2Project/rest/";

$(document).ready(function() {
	$("#upload_btn").click(function() {
		var fileIn = $("#fileToUpload")[0];
		// Has any file been selected yet?
		if (fileIn.files === undefined || fileIn.files.length == 0) {
			alert("Please select a file");
			return;
		}

		// We will upload only one file in this demo
		var file = fileIn.files[0];
		// Show the progress bar
		$("#progressbar").show();

		$.ajax({
			url : rootUrl + "dataset/upload?fileName=" + file.name + "&mimeType=" + file.type,
			type : "POST",
			data : file,
			processData : false, // Work around #1
			contentType : file.type, // Work around #2
			success : function() {
				$("#progressbar").hide();
			},
			error : function() {
				alert("Failed");
			},
			// Work around #3
			xhr : function() {
				myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('progress', showProgress, false);
				} else {
					console.log("Upload progress is not supported.");
				}
				return myXhr;
			}
		});
	});
	
	$("#progressbar").progressbar();
	$("#progressbar").hide();
});

function showProgress(evt) {
	if (evt.lengthComputable) {
		var percentComplete = (evt.loaded / evt.total) * 100;
		$('#progressbar').progressbar("option", "value", percentComplete);
	}
}