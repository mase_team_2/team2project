package com.team2.application;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.team2.user.businesslogic.TestUserDAO;
import com.team2.user.businesslogic.TestUserWS;
import com.team2.user.entities.TestUser;

@RunWith(Suite.class)
@SuiteClasses({ TestUser.class, TestUserDAO.class, TestUserWS.class })
public class AllTests {

}
