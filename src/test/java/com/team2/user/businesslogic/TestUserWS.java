package com.team2.user.businesslogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.team2.user.businesslogic.UserDAO;
import com.team2.user.businesslogic.UserWS;
import com.team2.user.entities.User;

public class TestUserWS {

	private UserWS userWS = new UserWS();

	@Before
	public void injectMockUserDAO() throws Exception {
		UserDAO userDAO = mock(UserDAO.class);
		userWS.setUserDAO(userDAO);
	}

	@Test
	public void testGetUserById() {
		User user = new User();
		user.setId("WH01A");

		when(userWS.getUserDAO().getUser("WH01A")).thenReturn(user);
		assertEquals(userWS.getUser("WH01A"), user);
	}

	@Test
	public void testGetUserByIdNotFound() {
		when(userWS.getUserDAO().getUser("WH01A")).thenReturn(null);
		assertNull(userWS.getUser("WH01A"));
	}

	@Test
	public void testAddUserSuccess() {
		User user = new User();
		user.setId("AA01A");

		String successMessage = "{\"message\":\"User " + user.getId()
				+ " successfully added to the database.\"}";
		assertEquals(successMessage, userWS.addUser(user));

		ArgumentCaptor<User> addedUser = ArgumentCaptor.forClass(User.class);
		verify(userWS.getUserDAO(), atLeast(0)).addUser(addedUser.capture());
	}

	@Test
	public void testAddUserFail() {
		User user = new User();
		user.setId("AA01A");

		when(userWS.getUserDAO().getUser(user.getId())).thenReturn(user);
		
		String failureMessage = "{\"message\":\"User " + user.getId()
				+ " already exists in the database.\"}";
		assertEquals(failureMessage, userWS.addUser(user));
	}
	
	@Test
	public void testAdminLoginOk() {
		String id = "williamheavin";
		String password = "password";

		User user = new User();
		user.setId(id);
		user.setPassword(password);
		user.setUserType("Administrator");

		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("OKAY Administrator", userWS.userLogin(id, password));
	}

	@Test
	public void testUserLoginWrongPassword() {
		String id = "williamheavin";
		String password = "password";
		String incorrectPassword = "test";

		User user = new User();
		user.setId(id);
		user.setPassword(password);

		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("FAIL", userWS.userLogin(id, incorrectPassword));
	}
	
	@Test
	public void testUserLoginUnknownUser() {
		String incorrectId = "someone";
		String incorrectPassword = "test";

		when(userWS.getUserDAO().getUser(incorrectId)).thenReturn(null);
		assertEquals("FAIL", userWS.userLogin(incorrectId, incorrectPassword));
	}

}
