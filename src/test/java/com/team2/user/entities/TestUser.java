package com.team2.user.entities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.team2.user.entities.User;

public class TestUser {

	private User user;

	@Before
	public void setUp() throws Exception {
		user = new User();
		assertNotNull(user);
	}

	@Test
	public void testSetGetID() {
		user.setId("WH01A");
		assertEquals("WH01A", user.getId());
	}

	@Test
	public void testSetGetPassword() {
		user.setPassword("password");
		assertEquals("password", user.getPassword());
	}

	@Test
	public void testSetGetFirstName() {
		user.setFirstName("William");
		assertEquals("William", user.getFirstName());
	}

	@Test
	public void testSetGetLastName() {
		user.setLastName("Heavin");
		assertEquals("Heavin", user.getLastName());
	}

	@Test
	public void testSetGetUserType() {
		user.setUserType("Administrator");
		assertEquals("Administrator", user.getUserType());
	}
}
