package com.team2.user.businesslogic;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.team2.user.entities.User;

@Stateless
@LocalBean
public class UserDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public User getUser(String id) {
		return entityManager.find(User.class, id);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addUser(User user) {
		entityManager.persist(user);
	}

	// Test methods
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
}
