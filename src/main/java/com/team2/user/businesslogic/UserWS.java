package com.team2.user.businesslogic;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.team2.user.entities.User;

@Path("/users")
@Stateless
@LocalBean
public class UserWS {

	@EJB
	private UserDAO userDAO;

	// returns user by given id
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public User getUser(@PathParam("id") String id) {
		return userDAO.getUser(id);
	}

	// adds new user to the database
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addUser(User user) {
		User fetchedUser = userDAO.getUser(user.getId());
		String message = "";

		// checks if user doesn't already exist in database
		if (fetchedUser == null) {
			userDAO.addUser(user);
			message = "{\"message\":\"User " + user.getId()
					+ " successfully added to the database.\"}";
		} else {
			message = "{\"message\":\"User " + user.getId()
					+ " already exists in the database.\"}";
		}
		return message;
	}

	// logs-in user by checking for user name and password
	@GET
	@Path("/login/{id}&{password}")
	@Produces({ MediaType.TEXT_PLAIN })
	public String userLogin(@PathParam("id") String id,
			@PathParam("password") String password) {

		// get user from database by id
		User user = userDAO.getUser(id);
		String loginMessage = "";

		// compare passwords
		if (isAuthenticated(user, password)) {
			loginMessage = "OKAY";
			
			// if the user is an admin
			if (user.getUserType().equals("Administrator")) {
				loginMessage += " Administrator";
			}
		} else {
			loginMessage = "FAIL";
		}
		return loginMessage;
	}

	private boolean isAuthenticated(User user, String password) {
		return user != null && user.getPassword().equals(password);
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
