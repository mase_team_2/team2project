package com.team2.dataset.businesslogic;

public enum TableType {
	BASE_DATA("Base Data"), EVENT_CAUSE("Event Cause Table"), FAILURE_CLASS(
			"Failure Class Table"), UE_TABLE("UE Table"), MCC_MNC(
			"MCC - MNC Table");

	private String tableName;

	private TableType(String tableName) {
		this.tableName = tableName;
	}

	public String getTableName() {
		return tableName;
	}
}
