package com.team2.dataset.businesslogic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.team2.dataset.entities.EntityObject;
import com.team2.dataset.validator.DataValidator;

@Stateless
@LocalBean
public class ExcelSheetReader {

	// private static ValidationClass validationClass = new ValidationClass();
	private static InvalidBaseDataExcelSheetWriter excelSheetWriter;

	@EJB
	private DatasetDAO dataPersistorDAO;

	// create entity from data object
	EntityObjectFactory entityObjectFactory = new EntityObjectFactory();

	public void readDataSheet(String fileName, String sheetName) {
		final File excelFile = new File(fileName);
		excelSheetWriter = new InvalidBaseDataExcelSheetWriter(sheetName);

		if (!excelFile.exists()) {
			System.out.println("File not found");
		}

		try {
			FileInputStream inputStream = new FileInputStream(excelFile);
			HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
			HSSFSheet workSheet = workBook.getSheet(sheetName);

			final int rowNum = workSheet.getLastRowNum();
			final int colNum = workSheet.getRow(0).getLastCellNum();

			for (int row = 1; row <= rowNum; row++) {
				final HSSFRow hssfRow = workSheet.getRow(row);
				HSSFCell[] dataObject = new HSSFCell[colNum];
				// boolean dataValid = true;

				for (int col = 0; col < colNum; col++) {
					final HSSFCell cell = hssfRow.getCell(col);
					dataObject[col] = cell;
				}

				// check if dataObject is valid
				boolean dataValid = DataValidator.validateData(dataObject,
						sheetName);

				if (dataValid) {
					// create entity object
					EntityObject entityObject = entityObjectFactory
							.createEntityObject(dataObject, sheetName);

					// persist object to database
					dataPersistorDAO.addData(entityObject);
				} else {
					System.out.println("Data not valid");
					excelSheetWriter.writeInvalidData(dataObject);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
