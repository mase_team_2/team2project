package com.team2.dataset.businesslogic;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.team2.dataset.entities.BaseData;

@Path("/dataset")
@Stateless
@LocalBean
public class DatasetWS {

	@EJB
	private ExcelSheetReader esr = new ExcelSheetReader();

	@EJB
	private DatasetDAO dataPersistorDAO;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> findAll() {
		return dataPersistorDAO.getAllBaseData();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String addData(@PathParam("id") String id) {
		try {
			final String DATA_TABLE = "Base Data";
			final String FILE_PATH = "data/dataset.xls";

			// Read excel file and create data object
			esr.readDataSheet(FILE_PATH, DATA_TABLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "{\"message\":\"added base data to database\"}";
	}
}
