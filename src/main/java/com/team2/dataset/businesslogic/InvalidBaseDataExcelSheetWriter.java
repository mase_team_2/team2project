package com.team2.dataset.businesslogic;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class InvalidBaseDataExcelSheetWriter {
	private static int rowCounter = 0;
	private String sheetName;

	public InvalidBaseDataExcelSheetWriter(String sheetName) {
		this.sheetName = "Invalid " + sheetName;

	}

	public void writeInvalidData(HSSFCell[] invalidDataObject) {
		if (sheetName.equals("Invalid Base Data")) {
			try {
				Workbook invalidDataWorkbook;
				Sheet invalidDataSheet;
				if (rowCounter == 0) {
					invalidDataWorkbook = new HSSFWorkbook();
					invalidDataSheet = invalidDataWorkbook
							.createSheet(sheetName);
				} else {
					FileInputStream fis = new FileInputStream("data/InvalidData.xls");
					invalidDataWorkbook = new HSSFWorkbook(fis);
					invalidDataSheet = invalidDataWorkbook.getSheet(sheetName);
				}

				FileOutputStream invalidDataOutputStream = new FileOutputStream(
						"InvalidData.xls");
				HSSFRow row = (HSSFRow) invalidDataSheet.createRow(rowCounter);

				for (int i = 0; i < invalidDataObject.length; i++) {
					HSSFCell cell = row.createCell(i);

					if (i == 3 || i == 10 || i == 11 || i == 12 || i == 13) {
						HSSFCell bigIntCell = invalidDataObject[i];
						bigIntCell.setCellType(Cell.CELL_TYPE_STRING);
						BigInteger bigInt = new BigInteger(
								bigIntCell.getStringCellValue());
						cell.setCellValue(bigInt.toString());
					} else {
						cell.setCellValue(invalidDataObject[i].toString());
					}

				}
				rowCounter++;

				invalidDataWorkbook.write(invalidDataOutputStream);
				invalidDataOutputStream.flush();
				invalidDataOutputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

}
