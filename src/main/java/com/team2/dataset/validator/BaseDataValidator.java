package com.team2.dataset.validator;


import java.math.BigInteger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;

public class BaseDataValidator implements Validators {
	
	public boolean validate(HSSFCell[] dataObject) {
		boolean dataValid = true;
		for (HSSFCell dataCell : dataObject) {
			if (dataCell.toString().equals("(null)")) {
				return false;
			}

		}
		if (isInvalidDateCell(dataObject[0])) {
			dataValid = false;
		}

		if (isInvalidEventIDCell(dataObject[1])) {
			dataValid = false;
		}


		if (isInvalidFailureClass(dataObject[2])) {
			dataValid = false;
		}

		if (isInvalidUEType(dataObject[3])) {
			dataValid = false;
		}

		if (isInvalidMarket(dataObject[4])) {
			dataValid = false;
		}

		if (isInvalidOperator(dataObject[5])) {
			dataValid = false;
		}

		if (isInvalidCellID(dataObject[6])) {
			dataValid = false;
		}

		if (isInvalidDuration(dataObject[7])) {
			dataValid = false;
		}

		if (isInvalidCauseCode(dataObject[8])) {
			dataValid = false;
		}

		if (isInvalidNEVersion(dataObject[9])) {
			dataValid = false;
		}

		if (isInvalidIMSI(dataObject[10])) {
			dataValid = false;
		}

		if (isInvalidHIER3_ID(dataObject[11])) {
			dataValid = false;
		}

		if (isInvalidHIER32_ID(dataObject[12])) {
			dataValid = false;
		}

		if (isInvalidHIER321_ID(dataObject[13])) {
			dataValid = false;
		}

		return dataValid;
	}

	private boolean isInvalidDateCell(HSSFCell dateCell) {

		return !HSSFDateUtil.isCellDateFormatted(dateCell);
	}

	private boolean isInvalidEventIDCell(HSSFCell eventIDCell) {
		
		int eventID = (int) eventIDCell.getNumericCellValue();
		return (eventID < 4000 || eventID > 5000);
	}

	private boolean isInvalidFailureClass(HSSFCell failureClassCell) {
		int failureClass = (int) failureClassCell.getNumericCellValue();
		return (failureClass < 0 || failureClass > 4);
	}

	private boolean isInvalidUEType(HSSFCell ueTypeCell) {
		int ueType = (int) ueTypeCell.getNumericCellValue();
		return (ueType < 20000000 || ueType > 40000000);

	}

	private boolean isInvalidMarket(HSSFCell marketCell) {
		int  market = (int) marketCell.getNumericCellValue();
		return (market<200||market>400);
	}

	private boolean isInvalidOperator(HSSFCell operatorCell) {
		int operator = (int)operatorCell.getNumericCellValue();
		return (operator<1||operator>1000);
	}

	private boolean isInvalidCellID(HSSFCell cellIDCell) {
		int cellID = (int) cellIDCell.getNumericCellValue();
		return (cellID<1||cellID>9999);
	}

	private boolean isInvalidDuration(HSSFCell durationCell) {
		int duration = (int) durationCell.getNumericCellValue();
		return duration<0;
	}

	private boolean isInvalidCauseCode(HSSFCell causeCodeCell) {
		int causeCode = (int) causeCodeCell.getNumericCellValue();
		return (causeCode<0||causeCode>30);
	}

	private boolean isInvalidNEVersion(HSSFCell neVersionCell) {
		String neVersion =neVersionCell.getStringCellValue();
		return !(neVersion.length()==3);
	}

	private boolean isInvalidIMSI(HSSFCell imsiCell) {
		imsiCell.setCellType(Cell.CELL_TYPE_STRING);
		BigInteger imsi = new BigInteger(imsiCell.getStringCellValue());
		return imsi.compareTo(new BigInteger("200000000000000"))<0||imsi.compareTo(new BigInteger("400000000000000"))>0;
	}

	private boolean isInvalidHIER3_ID(HSSFCell hier3_IDCell) {
		hier3_IDCell.setCellType(Cell.CELL_TYPE_STRING);
		BigInteger hier3_ID = new BigInteger(hier3_IDCell.getStringCellValue());
		return hier3_ID.compareTo(new BigInteger("100000000000000000"))<0||hier3_ID.compareTo(new BigInteger("9999999999999999999"))>0;
	}

	private boolean isInvalidHIER32_ID(HSSFCell hier32_IDCell) {
		hier32_IDCell.setCellType(Cell.CELL_TYPE_STRING);
		BigInteger hier32_ID = new BigInteger(hier32_IDCell.getStringCellValue());
		return hier32_ID.compareTo(new BigInteger("1000000000000000000"))<0||hier32_ID.compareTo(new BigInteger("9999999999999999999"))>0;
	}

	private boolean isInvalidHIER321_ID(HSSFCell hier321_IDCell) {
		hier321_IDCell.setCellType(Cell.CELL_TYPE_STRING);
		BigInteger hier321_ID = new BigInteger(hier321_IDCell.getStringCellValue());
		return hier321_ID.compareTo(new BigInteger("1000000000000000000"))<0||hier321_ID.compareTo(new BigInteger("9999999999999999999"))>0;
	}
}
