
$(function() {
	$('#loginForm').submit(function() {
		var userId = $("#userId").val();
		var password = $("#password").val();

		//user authentication 
		var responseMessage = authenticate(userId, password);
		
		//handle response  (may be handled on server side?)
		if (responseMessage == 'OKAY Administrator') { 
			window.location.replace("employee_create.html");
			return false;
		} 
		else if (responseMessage == 'FAIL') {
			// append login failed message to login page
			$('#error').append("User Login Failed: Wrong Username or Password");
		} else { // may be redundent
			// append error message to login page
			$('#error').append("Oops! Something went wrong there.");
		}
		return false;
	});
});

var authenticate = function(userId, password) { 
	var responseText = '';

	$.ajax({ 
		type: 'GET', 
		url: "/Team2Project/rest/users/login/" + userId + "&" + password, 
		dataType: 'text', 
		contentType: 'text/plain', 
		async: false, 
		success: function(text) { 
			responseText = text; 
		}, 
		error: function(text) { 
			responseText = text; 
		} 
	}); 
	return responseText; 
};